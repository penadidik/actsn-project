# Access Control Tournament Silat National
---

Application with SpringBoot, Kotlin, JS and the AdminLTE 2 template to facilitate development.


characteristics
---

* Spring Boot;
* Spring Security for basic login with permissions;
* Thymeleaf as view Template Engine;
* Kotlin;
* Javascript;
* LTE template bootstrap;
* Postgress as Database or others;
* Basic CRUD;
* High Level Security;

Run the application:
---
We assume that You already installed maven and JDK 8 (minimum) on your environment.
1. Clone the repository: `git clone https://bitbucket.org/penadidik/actsn.git`
2. Go to the folder: `cd actsn`
3. Install maven after download to `https://maven.apache.org/download.cgi`
4. Open command prompt and type `mvn clean spring-boot:run` to run
5. Open Your favorite browser, then go to http://localhost:8080

It is free to make changes and implementations and make the code more evolved.

| #   | Username | Password |  Status |
| --- |:--------:| --------:|--------:|
| 1   | naruto    | fisabilillah1703     | Super Admin |
| 2   | sazuke   | 1234   | Admin |
| 3   | ball         | 1234   | Operator |

## Screenshot

Login Page
![Login Page](data/img/login.PNG "Login Page")

Dashboard Page
![Dashboard Page](data/img/beranda.PNG "Dashboard Page")

Live Competition Page
![Live Competition Page](data/img/live_pertandingan.PNG "Live Competition Page")

Stage Competition Page
![Stage Competition Page](data/img/gelanggang_pertandingan.PNG "Stage Competition Page")

Participants
![Participants](data/img/participants.PNG "Participant")
![Add Edit Participant](data/img/add_edit_participant.PNG "Add Edit Participant")
![After Add Participant](data/img/after_add_participant.PNG "After Add Participant")

Competitions
![Competitions](data/img/add_edit_competition.PNG "Competitions")
![Add Edit Competition](data/img/add_competition.PNG "Add Edit Competition")

Result Page
![Result Page](data/img/result.PNG "Result Page")

Report Page
![Report Page](data/img/report.PNG "Report Page")

User Page
![User Page](data/img/user.PNG "User Page")

Role Page
![Role Page](data/img/role.PNG "Role Page")

User Role Page
![User Role Page](data/img/user_role.PNG "User Role Page")

Setting Menu Page
![Setting Menu Page](data/img/menu.PNG "Setting Menu Page")

Logout Button
![Logout](data/img/logout.PNG "Logout Page")

---
Lintaspena.org - Development Team
* website : http://lintaspena.org
* contact : 087812538105
* email  : admin@lintaspena.org
* If you interested this program call us.